const { City } = require('./City')
const { Country } = require('./Country')

let ukraine = new Country('Ukraine')

let city1 = new City('Kharkiv');
let city2 = new City('Lviv');
let city3 = new City('Lutsk');
let city4 = new City('Odesa');
let city5 = new City('Rivne');
let city6 = new City('Poltava');
let city7 = new City('Sumy');
let city8 = new City('Cherkasy');
let city9 = new City('Ternopil');
let city10 = new City('Kyiv');


city1.setForecast().then(() => {
    ukraine.addCity(city1);
}).catch((error) => { console.log(error) });

city2.setForecast().then(() => {
    ukraine.addCity(city2);
}).catch((error) => { console.log(error) });

city3.setForecast().then(() => {
    ukraine.addCity(city3);
}).catch((error) => { console.log(error) });

city4.setForecast().then(() => {
    ukraine.addCity(city4);
}).catch((error) => { console.log(error) });

city5.setForecast().then(() => {
    ukraine.addCity(city5);
}).catch((error) => { console.log(error) });

city6.setForecast().then(() => {
    ukraine.addCity(city6);
}).catch((error) => { console.log(error) });

city7.setForecast().then(() => {
    ukraine.addCity(city7);
}).catch((error) => { console.log(error) });

city8.setForecast().then(() => {
    ukraine.addCity(city8);
}).catch((error) => { console.log(error) });

city9.setForecast().then(() => {
    ukraine.addCity(city9);
}).catch((error) => { console.log(error) });

city10.setForecast().then(() => {
    ukraine.addCity(city10);

}).catch((error) => { console.log(error) });


function mySortDecline(a, b) {
    if (a.temperature > b.temperature)
        return 1
    if (a.temperature < b.temperature)
        return -1
    return 0
};




    ukraine.cities.sort(mySortDecline);
    console.log(ukraine.cities);


    //ukraine.removeCity('Lviv');
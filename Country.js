const { City } = require('./City');


class Country {
    constructor(name) {
        this.name = name;
        this.cities = [];
    }

    addCity(nameCity) {
        this.cities.push(nameCity);
    };

    removeCity(nameCity) {
        let cityIndex = this.cities.findIndex(currentValue => currentValue == nameCity);
        this.cities.splice(cityIndex, 1);
    }

}




module.exports = { Country };
const axios = require('axios')

const weatherUrl = 'https://goweather.herokuapp.com/weather/'

class Weather {
    /**
     * Describe attributes: temperature, wind, date
     */
    constructor() {
        this.temperature = ''
        this.wind = ''
        this.forecast = ''
        this.date = Date.now()
    }

    async setWeather(cityName) {
        await axios.get(weatherUrl + cityName).then((response) => {
            this.temperature = response.data.temperature
            this.wind = response.data.wind
        }).catch((error) => { return error.message })
    }

    async setForecast(cityName) {
        await axios.get(weatherUrl + cityName).then((response) => {
            this.temperature = response.data.temperature
            this.wind = response.data.wind
            this.forecast = `Day: ${response.data.forecast[0].day}
            Temperature: ${response.data.forecast[0].temperature}
            Wind: ${response.data.forecast[0].wind}`
        }).catch((error) => { return error.message })
    }

}

module.exports = { Weather }